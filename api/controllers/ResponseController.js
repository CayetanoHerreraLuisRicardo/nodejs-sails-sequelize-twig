/**
 * PostsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
module.exports = {
    getAll: function (req, res) {
        Response.findAll({ where: { post_id: req.param('id')} })
            .then(responses => {
                res.json(responses);
            }).catch(error => {
                res.status(500).json(error);
            });
    },
};