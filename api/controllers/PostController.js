/**
 * PostsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var Q = require('q');
module.exports = {
    getAll: function (req, res) {
        Q()
            .then(primeraFuncion)
            .then(function (val) {
                console.log("primeraFuncion=> ", val);
            })
            .then(segundaFuncion)
            .then(function (val) {
                console.log("segundaFuncion=> ", val);
                res.view('home', {
                    value: 5
                });
            })
            .catch(function (error) {
                console.log(error);
            })
            .done();
        console.log("last line");
    },
    getAll2: function (req, res) {
        Post.all().then(posts => {
            res.json(posts);
        });
    },
    getAll3: function (req, res) {
        Post.all().then(posts => {
            res.view('account/post', {
                posts: posts
            });
        });
    },
    getById: function (req, res) {
        Post.findById(req.param('id'))
            .then(post => {
                res.view('account/postDetails', {
                    post: post
                });
            })
            .catch(error => {
                res.redirect('/');
            });
    },
};

var primeraFuncion = function () {

    var deferred = Q.defer();

    setTimeout(function () {

        deferred.resolve(2);

    }, 2000);

    return deferred.promise;

}

var segundaFuncion = function () {

    var deferred = Q.defer();

    setTimeout(function () {


        deferred.resolve(1);

    }, 1000);

    return deferred.promise;

}

